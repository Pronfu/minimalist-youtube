# Minimalist Youtube

A minimalist version of YouTube that loads quickly and doesn't have the tracking that Youtube.com has.

## Installation

Clone or download repo, and put into your folder of choice. Then open either in your browser or copy to your web server.

## Live Demo
[https://projects.gregoryhammond.ca/minimalist-youtube/](https://projects.gregoryhammond.ca/minimalist-youtube/)

## License
[Unlicense](http://unlicense.org/)